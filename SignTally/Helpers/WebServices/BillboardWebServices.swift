//
//  BillboardWebServices.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import Alamofire

class BillboardWebServices{
    class func getBaseCost(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: User?)->()) {
        print(BASE_COST_URL)
        request(BASE_COST_URL, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<User>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
    class func createValuation(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: BillboardDetail?)->()) {
        print("\(BASE_URL)\(WEB_SERVICE_URL.billboard)")
        request("\(BASE_URL)\(WEB_SERVICE_URL.billboard)", method: .post, parameters: params, encoding: URLEncoding.httpBody).responseObject { (response: DataResponse<BillboardDetail>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
    class func updateValuation(_ params : Parameters, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: BillboardDetail?)->()) {
        print("\(BASE_URL)\(WEB_SERVICE_URL.billboard)")
        request("\(BASE_URL)\(WEB_SERVICE_URL.billboard)", method: .put, parameters: params, encoding: URLEncoding.httpBody).responseObject {(response: DataResponse<BillboardDetail>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
}
