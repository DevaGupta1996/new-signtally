//
//  Prefrences.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let USER_ID = "kUserID"
    static let USER_FIRST_NAME = "kUserName"
    static let USER_EMAIL = "kUserEmail"
    static let USER_PASSWORD = "kUserPassword"
    static let USER_PROFILE_PICTURE = "kUserProfilePicture"
    static let USER_TOKEN = "kUserLoginToken"
    static let USER_CREATED_ON = "kUserCreatedOn"
    static let USER_DEVICE_TOKEN = "kdevice_token"
    static let USER_EMAIL_VERIFIED = "kis_email_verified"
    static let USER_IS_DELETED = "kis_deleted"
    static let USER_LAST_NAME = "kLastName"
}

class Preferences{
    private static let defaults = UserDefaults.standard
    
    fileprivate class func save(_ value:Any?,_ key: String) {
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    
    class func removeAll(){
        let dic = defaults.dictionaryRepresentation()
        for (key,_) in dic {
            defaults.removeObject(forKey:key)
        }
        defaults.synchronize()
    }
    
    fileprivate class func get(key: String?) -> Any? {
        guard let key = key else {
            return nil
        }
        return defaults.value(forKey: key)
    }
    
    fileprivate class func delete(key: String?) {
        guard let key = key else {
            return
        }
        defaults.removeObject(forKey: key)
        defaults.synchronize()
    }
    
    
    
    
    //MARK: UserID
    class func removeUserID() {
        delete(key: Constants.USER_ID)
    }
    
    class func setUserID(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_ID)
    }
    
    class func getUserID() -> String? {
        guard let name = get(key: Constants.USER_ID) as? String else {
            return nil
        }
        return name
    }
    
    //MARK: UserNameEnglish
    class func removeFirstName() {
        delete(key: Constants.USER_FIRST_NAME)
    }
    
    class func setFirstName(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_FIRST_NAME)
    }
    
    class func getFirstName() -> String? {
        guard let name = get(key: Constants.USER_FIRST_NAME) as? String else {
            return nil
        }
        return name
    }
    
    
    //MARK: Email
    class func removeEmail() {
        delete(key: Constants.USER_EMAIL)
    }
    
    class func setEmail(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_EMAIL)
    }
    
    class func getEmail() -> String? {
        guard let name = get(key: Constants.USER_EMAIL) as? String else {
            return nil
        }
        return name
    }
    
    //MARK: Email
    class func removePassword() {
        delete(key: Constants.USER_PASSWORD)
    }
    
    class func setPassword(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_PASSWORD)
    }
    
    class func getPassword() -> String? {
        guard let name = get(key: Constants.USER_PASSWORD) as? String else {
            return nil
        }
        return name
    }
    
    //MARK: Profile Picture
    class func removeProfilePicture() {
        delete(key: Constants.USER_PROFILE_PICTURE)
    }
    
    class func setProfilePicture(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_PROFILE_PICTURE)
    }
    
    class func getProfilePicture() -> String? {
        guard let name = get(key: Constants.USER_PROFILE_PICTURE) as? String else {
            return nil
        }
        return name
    }
    
    //MARK: Last NAme
    class func removeLastName() {
        delete(key: Constants.USER_LAST_NAME)
    }
    
    class func setLastName(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_LAST_NAME)
    }
    
    class func getLastName() -> String? {
        guard let name = get(key: Constants.USER_LAST_NAME) as? String else {
            return nil
        }
        return name
    }
    
    //MARK: Latitude
    class func removeCreatedOn() {
        delete(key: Constants.USER_CREATED_ON)
    }
    
    class func setCreatedOn(name: String?) {
        guard let name = name else {
            return
        }
        save(name, Constants.USER_CREATED_ON)
    }
    
    class func getCreatedOn() -> String? {
        guard let name = get(key: Constants.USER_CREATED_ON) as? String else {
            return nil
        }
        return name
    }
    
//    //MARK: Longitude
//    class func removeLongitude() {
//        delete(key: Constants.USER_CUSTOM_LONG)
//    }
//    
//    class func setLongitude(name: Double?) {
//        guard let name = name else {
//            return
//        }
//        save(name, Constants.USER_CUSTOM_LONG)
//    }
//    
//    class func getLongitude() -> Double? {
//        guard let name = get(key: Constants.USER_CUSTOM_LONG) as? Double else {
//            return nil
//        }
//        return name
//    }
//    
//    
//    //MARK: Location
//    class func removeLocation() {
//        delete(key: Constants.USER_CUSTOM_LOCATION)
//    }
//    
//    class func setLocation(name: String?) {
//        guard let name = name else {
//            return
//        }
//        save(name, Constants.USER_CUSTOM_LOCATION)
//    }
//    
//    class func getLocation() -> String? {
//        guard let name = get(key: Constants.USER_CUSTOM_LOCATION) as? String else {
//            return nil
//        }
//        return name
//    }
}

