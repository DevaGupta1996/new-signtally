//
//  Designables.swift
//  HeroNavigation
//
//  Created by orangemac05 on 18/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import UIKit


private var __maxLengths = [UITextField: Int]()

@IBDesignable
extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            guard let length = __maxLengths[self] else {
                return 50
            }
            return length
        }
        
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix(textField:)), for: .editingChanged)
        }
    }
    
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
    
    @IBInspectable var leftViewImage: UIImage? {
        set {
            if newValue != nil {
                let view = UIView(frame: CGRect(x: 10, y: 0, width: frame.height + 20, height: frame.height))
                let imgView = UIImageView(frame: CGRect(x: view.bounds.origin.x + 15, y: view.bounds.origin.y + 10, width: frame.height - 20, height: frame.height - 20))
                imgView.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                imgView.image = newValue!
                imgView.contentMode = .scaleAspectFit
                imgView.clipsToBounds = true
                view.addSubview(imgView)
                self.leftView = view
                self.leftViewMode = .always
            }
            
        }
        
        get {
            return self.leftViewImage
        }
    }
    
    
    @IBInspectable var placeholderColor: UIColor {
        set {
            if newValue != .clear {
                self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor : newValue])
            }
        }
        
        get {
            return self.placeholderColor
        }
    }
    
    @IBInspectable var leftViewWidth: CGFloat {
        set {
            if newValue > 0.0 {
                let view = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.height))
                self.leftViewMode = .always
                self.leftView = view
            }
        }
        get {
            return self.leftViewWidth
        }
    }
    
    
    @IBInspectable var rightViewWidth: CGFloat {
        set {
            if newValue > 0.0 {
                let view = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: self.frame.height))
                self.rightViewMode = .always
                self.rightView = view
            }
        }
        
        get {
            return self.rightViewWidth
        }
    }
}



@IBDesignable
extension UIView {

    
    @IBInspectable var cornerRadi: CGFloat {
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = true
        }
        
        get {
            return self.cornerRadi
        }
    }
    
    
    @IBInspectable var borderColor: UIColor {
        set {
            self.layer.borderColor = newValue.cgColor
            self.layer.borderWidth = 0.5
        }
        
        get {
            return self.borderColor
        }
    }
    
    @IBInspectable var endEditing: Bool {
        set {
            if newValue {
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
                addGestureRecognizer(tapGesture)
            }
        }
        get {
            return self.endEditing
        }
    }
    
    @objc func hideKeyboard(){
        endEditing(true)
    }
}

//@IBDesignable
//class EdgeInsetLabel: UILabel {
//    var textInsets = UIEdgeInsets.zero {
//        didSet { invalidateIntrinsicContentSize() }
//    }
//
//    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
//        let insetRect = UIEdgeInsetsInsetRect(bounds, textInsets)
//        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
//        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
//                                          left: -textInsets.left,
//                                          bottom: -textInsets.bottom,
//                                          right: -textInsets.right)
//        return UIEdgeInsetsInsetRect(textRect, invertedInsets)
//    }
//
//    override func drawText(in rect: CGRect) {
//        super.drawText(in: UIEdgeInsetsInsetRect(rect, textInsets))
//    }
//}
//
//extension EdgeInsetLabel {
//    @IBInspectable
//    var leftTextInset: CGFloat {
//        set { textInsets.left = newValue }
//        get { return textInsets.left }
//    }
//
//    @IBInspectable
//    var rightTextInset: CGFloat {
//        set { textInsets.right = newValue }
//        get { return textInsets.right }
//    }
//
//    @IBInspectable
//    var topTextInset: CGFloat {
//        set { textInsets.top = newValue }
//        get { return textInsets.top }
//    }
//
//    @IBInspectable
//    var bottomTextInset: CGFloat {
//        set { textInsets.bottom = newValue }
//        get { return textInsets.bottom }
//    }
//}

extension String {
    func safelyLimitedTo(length n: Int) -> String {
        if self.count <= n {
            return self
        }
        return String(Array(self).prefix(upTo: n))
    }
}


extension Date {
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        return end - start
    }
    
    func getHumanReadable() -> String? {
        let format = DateFormatter()
        format.dateStyle = .long
        return format.string(from: self)
    }
    
    func getHumanReadableShortHand() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateStyle = .short
        return format.string(from: self)
    }
    
    func getHumanReadableShort() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateStyle = .medium
        return format.string(from: self)
    }
    
    func getHumanReadableMedium() -> String? {
        let format = DateFormatter()
        format.dateStyle = .medium
        return format.string(from: self)
    }
    
    func shortHandDateFormatting() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateFormat = "MMM d, h:mm a"
        return format.string(from: self)
    }
    
    func shortHandDateFormat() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateFormat = "dd MMMM, YYYY"
        return format.string(from: self)
    }
    
    func shortHandDateTimeFormat() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateFormat = "YYYYmmdd_hhmmss"
        return format.string(from: self)
    }
    
    func shortHandDateFormatForVideo() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateFormat = "dd-MM-YYYY"
        return format.string(from: self)
    }
    
    
    func dateFormatForSchedule() -> String? {
        let format = DateFormatter()
        format.locale = Locale(identifier: "en_US")
        format.dateFormat = "YYYY-MM-dd"
        return format.string(from: self)
    }
    
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date)   > 0 {
            if years(from: date) ==  1 {
                return "a year ago"
            }else {
                
                return "\(years(from: date)) years ago"
            }
        }
        if months(from: date) > 0 {
            if months(from: date) == 1 {
                return "a month ago"
            }else {
                return "\(months(from: date)) months ago"
            }
        }
        
        if days(from: date) > 0 {
            if days(from: date) == 1 {
                return "a day ago"
            }else {
                return "\(days(from: date)) days ago"
            }
        }
        
        if hours(from: date) > 0 {
            if hours(from: date) == 1 {
                return "an hour ago"
            }else {
                return "\(hours(from: date)) hours ago"
            }
        }
        
        if minutes(from: date) > 0 {
            if minutes(from: date) == 1 {
                return "a minute ago"
            }else {
                return "\(minutes(from: date)) minutes ago"
            }
        }
        
        return "few seconds ago"
    }
}
