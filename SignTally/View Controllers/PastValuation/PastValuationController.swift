//
//  PastValuationController.swift
//  SignValue
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//
import UIKit
import SVProgressHUD
import GoogleMaps
import CoreLocation


class PastValuationController: UIViewController {
    @IBOutlet weak var valuationInfoLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var valuationsTableView: UITableView!
    @IBOutlet weak var placeSearchTextField: UITextField!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var noValuationLabel: UILabel!
    
    //    var mapView : GMSMapView?
    var locationManager = CLLocationManager()
    var currnetLocation = CLLocation()
    //    var placePredections = [Place]()
    var pastValuationData : PastValiuations?
    var geocoder = GMSGeocoder()
    var zipcode = ""
    var city = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.valuationsTableView.tableFooterView = UIView()
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: OperationQueue.current) { (notification) in
            self.setupLocationManager()
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "LOCATION"), object: nil, queue: OperationQueue.current) { (notification) in
            if notification.userInfo != nil{
                if let zip = notification.userInfo?["zipcode"]as? String{
                    self.city = zip
                }
                if let locationName = notification.userInfo?["locationName"]as? String{
                    self.placeSearchTextField.text = locationName
                }
            }
            self.getLocationWiseValuation()
            //            print(">>>>>>>>\(self.zipcode),\(self.placeSearchTextField.text!)")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setupLocationManager()
        // self.setupMapView()
        //        self.getPastValuations()
    }
    
    
    @IBAction func homeLocationAction(_ sender: UIButton) {
        //        self.searchResultTableView.alpha = 0
        //        self.placePredections.removeAll()
        //        self.placeSearchTextField.text = ""
        //        self.placeSearchTextField.resignFirstResponder()
        //        self.mapView?.animate(toLocation: self.currnetLocation.coordinate)
        self.googleReversedGeocode(self.currnetLocation.coordinate)
        
    }
    
    @IBAction func mapSearchAction(_ sender: UIButton) {
        if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.MAP_IDENTIFIER)as? MapController{
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setupLocationManager(){
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        self.locationManager.distanceFilter = 100
        self.locationManager.startUpdatingLocation()
        self.getPastValuations()
    }
    
    //    func setupMapView(){
    //        self.mapView = GMSMapView(frame: CGRect(x: 0, y: 0, width: self.mapContainerView.frame.width, height: self.mapContainerView.frame.height))
    //        self.mapView?.isMyLocationEnabled = true
    //        self.mapView?.delegate = self
    //        self.mapContainerView.insertSubview(self.mapView!, at: 0)
    //    }
    
    
    func getPastValuations(){
        SVProgressHUD.show()
        PastValuationWebServices.getPastValuation(USER_ID(), self.city) {(success, error, dataDictPast) in
            SVProgressHUD.dismiss()
            if success{
                //                self.mapContainerView.alpha = 0
                self.valuationsTableView.alpha = 1
                if let data = dataDictPast{
                    if let finalData = data.data{
                        let commaSeprated = getCommaSepratedNumber(finalData.totalPrice)
                        print(finalData.totalPrice)
                        self.valuationInfoLabel.text = "\(finalData.no_of_evaluation) Valuations, Worth $\(commaSeprated)"
                        self.dateLabel.text = "Since"
                        if let index = finalData.date.range(of: "T")?.lowerBound {
                            let substring = finalData.date[..<index]
                            let string = String(substring)
                            if let date = dateSignTally(fromString: string){
                                if let finalDate = date.getHumanReadableMedium(){
                                    
                                    self.dateLabel.text = finalDate
                                    
                                    self.dateLabel.text = "Since \(finalDate)"
                                }
                            }
                        }
                    }else{
                        self.valuationInfoLabel.text = ""
                        self.dateLabel.text = ""
                    }
                }
            }else{
                print(error!)
            }
        }
    }
    
    
    func getLocationWiseValuation(){
        SVProgressHUD.show()
        PastValuationWebServices.getPastValuationLocationWise(USER_ID(), self.city) { (success, error, dataDictPast) in
            SVProgressHUD.dismiss()
            if success{
                //                self.mapContainerView.alpha = 0
                //                self.valuationsTableView.alpha = 1
                if let data = dataDictPast{
                    self.pastValuationData = data
                    self.valuationsTableView.reloadData()
                    if data.dataSecond.count > 0{
                        self.valuationsTableView.reloadData()
                        self.valuationsTableView.alpha = 1
                        self.noValuationLabel.alpha = 0
                    }else{
                        self.valuationsTableView.alpha = 0
                        self.noValuationLabel.alpha = 1
                        //showAlert(title: "Sign Tally", message: "No data found for this location", buttonTitle: "Ok", vc: self)
                    }
                    
                    //
                    //                    if let finalData = data.data{
                    //                        self.valuationInfoLabel.text = "\(finalData.no_of_evaluation) Valuations, Worth $\(finalData.totalPrice)"
                    //                        if let index = finalData.date.range(of: "T")?.lowerBound {
                    //                            let substring = finalData.date[..<index]
                    //                            let string = String(substring)
                    //                            if let date = dateSignTally(fromString: string){
                    //                                if let finalDate = date.getHumanReadableMedium(){
                    //
                    //                                    self.dateLabel.text = finalDate
                    //
                    //                                    self.dateLabel.text = "Since \(finalDate)"
                    //                                }
                    //                            }
                    //                        }
                    //                    }else{
                    //                        self.valuationInfoLabel.text = ""
                    //                        self.dateLabel.text = ""
                    //                    }
                }
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "OK", vc: self)
            }
        }
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    //
}


extension PastValuationController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        if tableView.isEqual(self.valuationsTableView){
        
        if let data = self.pastValuationData?.dataSecond{
            if data.count > 0{
                return data.count
            }
        }
        //        }else{
        //            if self.placePredections.count > 0{
        //                return self.placePredections.count
        //            }
        //        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        if tableView.isEqual(self.valuationsTableView){
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)as! PastValuationCell
        if let data = self.pastValuationData?.dataSecond{
            let finalData = data[indexPath.row]
            cell.companyNameLabel.text = finalData.other_company
            
            if let index = finalData.created_on.range(of: "T")?.lowerBound {
                let substring = finalData.created_on[..<index]
                let string = String(substring)
                print(string)
                if let date = dateSignTally(fromString: string){
                    if let finalDate = date.getHumanReadableMedium(){
                        cell.structureTypeAndDateLabel.text = "\(finalData.structure_type) , \(finalDate)"
                    }
                }
                //                    if let finalDate = date.getHumanReadableMedium(){
                //                        cell.structureTypeAndDateLabel.text = "\(finalData.structure_type) , \(finalDate)"
                //                    }
            }
            if finalData.photo_url != ""{
                cell.locationImageView.af_setImage(withURL: URL(string: finalData.photo_url)!)
            }
        }
        return cell
        //        }else{
        //            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        //            let data = self.placePredections[indexPath.row]
        //            if let structure = data.structuredFormat{
        //                cell.textLabel?.text = structure.main_text
        //                cell.detailTextLabel!.text = structure.secondary_text
        //            }
        //            return cell
        //        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if tableView.isEqual(self.valuationsTableView){
        if let data = self.pastValuationData?.dataSecond{
            let finalData = data[indexPath.row]
            openSafariController(self, finalData.document_url, VIEW_CONTROLLER_IDENTIFIER.PAST_VALUATIONS_IDENTIFIER)
//            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.SIGN_RESULT_DISPLAY_IDENTIFIER)as? ResultDisplayController{
//                vc.billboardData = finalData
//                vc.source = VIEW_CONTROLLER_IDENTIFIER.PAST_VALUATIONS_IDENTIFIER
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
        }
        //        }else{
        //            let searchPlace = self.placePredections[indexPath.row]
        //            print(searchPlace)
        //            self.getPlaceCoordinates(searchPlace.description)
        //        }
    }
    
    func getPlaceCoordinates(_ placeDescription : String){
        GoogleWebService.getPlaceCoordinates(placeName: placeDescription) { (success, error, coordinate) in
            if success{
                if let data = coordinate?.results[0].geometry{
                    if let latLong = data.location{
                        print(latLong.Latitude,latLong.Longitude)
                        //                        self.searchResultTableView.alpha = 0
                        let coordinates = CLLocationCoordinate2DMake(latLong.Latitude, latLong.Longitude)
                        //                        self.mapView?.animate(toLocation: coordinates)
                    }
                }
            }else{
                print(error!)
            }
        }
    }
}


extension PastValuationController : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestAlwaysAuthorization()
            break
        case .authorizedAlways,.authorizedWhenInUse:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted ,.denied:
            let alert = UIAlertController(title: "Location permissions are disabled for this application", message: "Please enable your location services", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            break
//        case  :
//            let alert = UIAlertController(title: "Device location services are disabled", message: "Please enable your location services", preferredStyle: .alert)
//            let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
//                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
//            }
//            alert.addAction(okAction)
//            present(alert, animated: true, completion: nil)
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            self.currnetLocation = location
            let locationCoordinates = location.coordinate
            //            let camera = GMSCameraPosition(target: locationCoordinates, zoom: 16.0, bearing: 0, viewingAngle: 0)
            self.googleReversedGeocode(locationCoordinates)
            //            self.mapView?.camera = camera
            //            self.mapView?.animate(toLocation: locationCoordinates)
        }
    }
    
    
    func googleReversedGeocode(_ location : CLLocationCoordinate2D ){
        
        geocode(latitude: location.latitude, longitude: location.longitude) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            // you should always update your UI in the main thread
            DispatchQueue.main.async {
                self.zipcode = placemark.postalCode ?? ""
                self.city = placemark.locality ?? ""
                
                print(self.city, self.zipcode)
                self.placeSearchTextField.text = "\(self.city), \(self.zipcode)"
                self.getLocationWiseValuation()
            }
        }

//        geocode(latitude: location.latitude, longitude: location.longitude) { placemark, error in
//            guard let placemark = placemark, error == nil else { return }
//            // you should always update your UI in the main thread
//            DispatchQueue.main.async {
//                //                self.streetNumberTextFiled.text = placemark.subThoroughfare ?? ""
//                //                self.streetNameTextField.text = placemark.thoroughfare ?? ""
//                //                self.locationNameLabel.text = placemark.locality ?? ""
//                //                print("zip code:", placemark.postalCode ?? "")
//                self.zipcode = placemark.postalCode ?? ""
//                let city = placemark.locality ?? ""
//                self.placeSearchTextField.text = "\(city),\(self.zipcode)"
//            }
//        }
        
//        SVProgressHUD.show()
//        geocoder.reverseGeocodeCoordinate(location) { (response, error) in
//            SVProgressHUD.dismiss()
//            if let locationData = response?.results(){
//                let address : GMSAddress = locationData[0]
//                if let locality = address.lines{
//                    let completeAddress = locality.map{String($0)}//.joined(separator: ",")
//                    self.placeSearchTextField.text = completeAddress.joined(separator: ",")
//                    //                    var locationArray = completeAddress[0].components(separatedBy: ",")
//                    //                    print(locationArray)
//                    //                    if locationArray[4] != ""{
//                    //                        let state = locationArray[4]
//                    //                        let statArray = state.components(separatedBy: " ")
//                    //                        print(statArray)
//                    //                        if statArray[2] != ""{
//                    //                            self.zipcode = statArray[2]
//                    //                        }else{
//                    //                            self.zipcode = ""
//                    //                        }
//                    //                        print(self.zipcode)
//                }
//            }
//            
//            print(error)
//        }
    }
}


//extension PastValuationController : UITextFieldDelegate{
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        let finalText = textField.text ?? "" + string
//        if finalText != ""{
//            let trimmedSearchText = textField.text!.replacingOccurrences(of: " ", with: "")
//            GoogleWebService.getPlaceList(searchString: trimmedSearchText) { (success, error, place) in
//                if success{
//                    if let data = place{
//                        self.placePredections = data.predictions
//                        if data.predictions.count > 0{
//                            self.searchResultTableView.alpha = 1
//                            self.mapContainerView.alpha = 1
//                            self.searchResultTableView.reloadData()
//                            self.valuationsTableView.alpha = 0
//                        }else{
//                            self.searchResultTableView.alpha = 0
//                            self.mapContainerView.alpha = 0
//                        }
//                    }
//                }else{
//                    self.searchResultTableView.alpha = 0
//                    print(error!)
//                }
//            }
//        }else{
//            self.searchResultTableView.alpha = 0
//        }
//        return true
//    }
//}


//extension PastValuationController : GMSMapViewDelegate{
//    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
//        let latitude = mapView.camera.target.latitude
//        let longitude = mapView.camera.target.longitude
//        //        let locationCoordinate2D = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
//        //        self.googleReversedGeocode(locationCoordinate2D)
//        geocode(latitude: latitude, longitude: longitude) { (placemark, error) in
//            guard let placemark = placemark, error == nil else { return }
//            DispatchQueue.main.async {
//                print("address1:", placemark.thoroughfare ?? "")
//                print("address2:", placemark.subThoroughfare ?? "")
//                print("city:",     placemark.locality ?? "")
//                print("state:",    placemark.administrativeArea ?? "")
//                print("zip code:", placemark.postalCode ?? "")
//                self.zipcode = placemark.locality ?? ""
//                print("country:",  placemark.country ?? "")
//                self.getLocationWiseValuation()
//            }
//        }
//    }
//}
