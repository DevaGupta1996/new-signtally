//
//  PastValuationCell.swift
//  SignValue
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class PastValuationCell: UITableViewCell {
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var structureTypeAndDateLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
