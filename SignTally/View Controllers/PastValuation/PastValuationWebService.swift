//
//  PastValuationWebService.swift
//  SignValue
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


class PastValuationWebServices{
    class func getPastValuation(_ userID : String ,_ zipcode : String, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: PastValiuations?)->()) {
        print("\(past_Valuation_API)/\(userID)")
        request("\(past_Valuation_API)/\(userID)", method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<PastValiuations>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
    class func getPastValuationLocationWise(_ userID : String ,_ zipcode : String, completion: @escaping (_ success: Bool,_ message: String?,_ pastValuationData: PastValiuations?)->()) {
        let params = [
            "user_id" : userID,
            "postal_code" : zipcode
        ]
        
        print(params,past_Valuation_API)
        request(past_Valuation_API, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted).responseObject { (response: DataResponse<PastValiuations>) in
            switch response.result {
            case .success(let val):
                if val.status == "200" {
                    completion(true, nil, val)
                }else {
                    completion(false, val.msg, nil)
                }
            case .failure(let error):
                completion(false, error.localizedDescription, nil)
            }
        }
    }
    
}
