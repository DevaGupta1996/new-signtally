//
//  PastValuationDictionary.swift
//  SignValue
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import ObjectMapper


struct PastValiuations : Mappable,Codable{
    var status = ""
    var msg = ""
    var data : PastData?
    var dataSecond = [BillboardData]()
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        status <- map["status"]
        msg <- map["message"]
        data <- map["data"]
        dataSecond <- map["data"]
    }
}



struct PastData : Mappable,Codable {
    var date = ""
    var totalPrice = 0.0
    var no_of_evaluation = 0
    var billboard_details = [BillboardData]()
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        date <- map["date"]
        totalPrice <- map["totalPrice"]
        no_of_evaluation <- map["no_of_evaluation"]
        billboard_details <- map["billboard_details"]
    }
}


struct Billboard : Codable , Mappable{
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        _id <- map["_id"]
        user_id <- map["user_id"]
        company_id <- map["company_id"]
        created_on <- map["created_on"]
        base_cost_int <- map["base_cost_int"]
        additional_cost_int <- map["additional_cost_int"]
        is_deleted <- map["is_deleted"]
        comments <- map["comments"]
        iso_date <- map["iso_date"]
        month <- map["month"]
        year_three_message_display <- map["year_three_message_display"]
        three_message_display <- map["three_message_display"]
        year_digital_built <- map["year_digital_built"]
        digital_display <- map["digital_display"]
        age <- map["age"]
        year_built_2 <- map["year_built_2"]
        year_built <- map["year_built"]
        illuminated <- map["illuminated"]
        display_element <- map["display_element"]
        base_cost <- map["base_cost"]
        additional_cost <- map["additional_cost"]
        display_size <- map["display_size"]
        hagl <- map["hagl"]
        structure_type <- map["structure_type"]
        state <- map["state"]
        street_name <- map["street_name"]
        street_no <- map["street_no"]
        parcel_no <- map["parcel_no"]
        county <- map["county"]
        zip_code <- map["zip_code"]
        photo_url_2 <- map["photo_url_2"]
        city <- map["city"]
        longitude <- map["longitude"]
        latitude <- map["latitude"]
        document_url <- map["document_url"]
        photo_url <- map["photo_url"]
        permit_number <- map["permit_number"]
        display_number <- map["display_number"]
        created_by <- map["created_by"]
        sub_type <- map["sub_type"]
        digital_display_rcnld <- map["digital_display_rcnld"]
        three_msg_display_rcnld <- map["three_msg_display_rcnld"]
        total_billboard_rcnld <- map["total_billboard_rcnld"]
        structure_rcnld <- map["structure_rcnld"]
        other_company <- map["other_company"]
        sigh <- map["sigh"]
        company_name <- map["company_name"]
    }
    
    
    var _id = ""
    var user_id = ""
    var company_id = ""
    var created_on = ""
    var base_cost_int = 0.0
    var additional_cost_int = 0.0
    var is_deleted = false
    var comments =  ""
    var iso_date = ""
    var month =  ""
    var year_three_message_display = ""
    var three_message_display = ""
    var year_digital_built = ""
    var digital_display = ""
    var age = ""
    var year_built_2 = ""
    var year_built = ""
    var illuminated = ""
    var display_element = ""
    var base_cost = ""
    var additional_cost = ""
    var display_size = ""
    var hagl = ""
    var structure_type = ""
    var state = ""
    var street_name = ""
    var street_no = ""
    var parcel_no = ""
    var county = ""
    var zip_code = ""
    var photo_url_2 = ""
    var city = ""
    var longitude = 0.0
    var latitude = 0.0
    var document_url = ""
    var photo_url = ""
    var permit_number = ""
    var display_number = ""
    var created_by = ""
    var sub_type = ""
    var digital_display_rcnld = ""
    var three_msg_display_rcnld = ""
    var total_billboard_rcnld = ""
    var structure_rcnld = ""
    var other_company = ""
    var sigh = ""
    var company_name = ""
}
