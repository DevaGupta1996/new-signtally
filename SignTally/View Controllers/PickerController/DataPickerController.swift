
//
//  HelpViewController.swift
//  SignTally
//
//  Created by orangemac05 on 30/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit

class DataPickerController: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource {

    @IBOutlet weak var genderPicker: UIPickerView!
    var genderData = [String]()
    var genderDelegate: DataPickerDelegate?
    var sourceView = ""
    var atRow = 0
   
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
     
        self.atRow = row
        genderDelegate?.pickupData(selected: genderData[row], selectedIndex: row,sourceName: self.sourceView)
    
        
    }
        
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func closeController(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneSelectionAction(_ sender: UIBarButtonItem) {
        genderDelegate?.pickupData(selected: genderData[atRow], selectedIndex: atRow, sourceName: self.sourceView)
        self.dismiss(animated: true, completion: nil)
    }
    
}
