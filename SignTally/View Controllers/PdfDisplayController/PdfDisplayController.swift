//
//  PdfDisplayController.swift
//  SignTally
//
//  Created by Orange on 27/11/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD
import NVActivityIndicatorView

class PdfDisplayController: UIViewController, NVActivityIndicatorViewable {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    var indicator: NVActivityIndicatorView!
    var pdfLink = ""
    var source = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show(withStatus: "creating a Report.")
        
//        self.indicator = NVActivityIndicatorView(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100), type: NVActivityIndicatorType.ballClipRotateMultiple, color: UIColor.red, padding: 10)
//        self.indicator.center = self.view.center
////
////        self.indicator.startAnimating(self.indicator.frame.size, message: "creating a pdf")
        
//        self.indicator?.startAnimating(size: CGSize(width: 100.0, height: 100.0),
//                                      message: "creating a pdf",
//                                      type: NVActivityIndicatorType.ballClipRotateMultiple,
//                                      color: UIColor.red,
//                                      padding: 10,
//                                      textColor: UIColor.darkGray)
        
        let webView = WKWebView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.containerView.frame.height))
        let url = URL(string: self.pdfLink)
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url!))
        self.containerView.addSubview(webView)
        
        if source == VIEW_CONTROLLER_IDENTIFIER.PAST_VALUATIONS_IDENTIFIER{
            self.editButton.setImage(#imageLiteral(resourceName: "Back"), for: .normal)
            self.doneButton.alpha = 0
        }else{
            self.editButton.setImage(#imageLiteral(resourceName: "Pencil"), for: .normal)
            self.doneButton.alpha = 1
        }
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.finaldata!["is_deleted"] = false
        
        SVProgressHUD.show()
        BillboardWebServices.updateValuation(appDelegate.finaldata!) { (success, error, billboardData) in
            SVProgressHUD.dismiss()
            if success{
                let alert = UIAlertController(title: "", message: "Saved Successfully", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: {
                        startNotifier("POPTOROOT")
                    })
                    
                }))
                self.present(alert, animated: true, completion: nil)
               
            }
        }
    }
    @IBAction func shareAction(_ sender: UIButton) {
        self.loadPDFAndShare()
    }
}


extension PdfDisplayController : WKNavigationDelegate{
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        SVProgressHUD.dismiss()
        self.savePdf()
    }
    
    func savePdf(){
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0].appending("/document.pdf")
        //        let paths = (NSSearchPathForDirectoriesInDomains((.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("documento.pdf")
        
        let pdfDoc = NSData(contentsOf:URL(string: self.pdfLink)!)
        fileManager.createFile(atPath: path as String, contents: pdfDoc as Data?, attributes: nil)
    }
    
    func loadPDFAndShare(){
        let fileManager = FileManager.default
        let documentoPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0].appending("/document.pdf")
        
        if fileManager.fileExists(atPath: documentoPath){
            let documento = NSData(contentsOfFile: documentoPath)
            let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [documento!], applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView=self.view
            present(activityViewController, animated: true, completion: nil)
        }else{
            print("Document was not found")
        }
    }
    
}
