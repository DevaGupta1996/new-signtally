//
//  SideMenuController.swift
//  HeroNavigation
//
//  Created by Orange on 22/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import Alamofire

class SideMenuController: UIViewController {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var sidemenuTableView: UITableView!
    
    var menuTitleArray = ["ADD SIGN","PAST VALUATIONS","CHANGE PASSWORD","HELP","PRIVACY POLICY","LOGOUT"]
    var imageArray = [#imageLiteral(resourceName: "Add Sign"),#imageLiteral(resourceName: "Past Valuation"),#imageLiteral(resourceName: "Change Password"),#imageLiteral(resourceName: "Email"),#imageLiteral(resourceName: "Privacy"),#imageLiteral(resourceName: "Side Logout")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.sidemenuTableView.tableFooterView = UIView()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let email = Preferences.getEmail(){
            
            self.usernameLabel.text = "  \(email)  "
        }else{
            self.usernameLabel.text = "  -  "
        }
    }
    
    
    
    @IBAction func profileButtonAction(_ sender: UIButton) {
        //        self.dismiss(animated: true) {
        //            startNotifier(SIDE_MENU_NOTIFIER.PROFILE)
        //        }
    }
}

extension SideMenuController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuTitleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)as! SideMenuCell
        cell.cellTitleLabel.text = self.menuTitleArray[indexPath.row]
        cell.cellImageView.image = self.imageArray[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.dismiss(animated: true) {
            switch indexPath.row{
            case 0:
                request(BASE_URL + "users/billboard/valuation/check/" + Preferences.getUserID()!, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: nil).validate().responseObject { (response: DataResponse<User>) in
                    switch response.result {
                    case .success(let val):
                        if val.status == "200" {
                            if let vc = MAIN_STORYBOARD.instantiateViewController(withIdentifier: VIEW_CONTROLLER_IDENTIFIER.ADD_SIGN_IDENTIFIER)as? AddSignController{
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }else {
                            let alert = UIAlertController(title: nil, message: val.msg, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                                self.dismiss(animated: true, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    case .failure(_):
                        print("api error")
                    }
                }
               // startNotifier(SIDE_MENU_NOTIFIER.ADD_SIGN)
                break
            case 1:
                startNotifier(SIDE_MENU_NOTIFIER.PAST_VALUATION)
                break
            case 2:
                startNotifier(SIDE_MENU_NOTIFIER.CHANGEPASSWARD)
                break
            case 3:
                startNotifier(SIDE_MENU_NOTIFIER.HELP)
                break
            case 4:
                startNotifier(SIDE_MENU_NOTIFIER.PRIVACY_POLICY)
                break
            case 5:
                startNotifier(SIDE_MENU_NOTIFIER.LOGOUT)
                break
            default:
                print("Menu closed")
                break
            }
            
        }
    }
    
    
}
