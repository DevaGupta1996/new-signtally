//
//  AddSign+Digital.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import Foundation
import UIKit

extension AddSignController{
    
    //digitalDisplayLabel yearDisplayBuiltLabel threeMsgDisplayLabel yearThreeMsgDisplayBuiltLabel
    
    func digitalViewValidations() -> String{
        if self.digitalDisplayLabel.text == "Digital Display" && self.yearDisplayBuiltLabel.text == "" && self.threeMsgDisplayLabel.text == "" && self.yearThreeMsgDisplayBuiltLabel.text == ""{
            return VALIDATION_ENUM.ALL_FIELD_VALIDATION_MESSEGE
        }else if self.digitalDisplayLabel.text == "Digital Display"{
            return VALIDATION_ENUM.DIGITAL_DISPLAY_VALIDATION
        }else if self.yearDisplayBuiltLabel.text == "Year Display Built" && self.digitalDisplayLabel.text != "0"{
            return VALIDATION_ENUM.YEAR_BUILD_DISPLAY_VALIDATION
        }else if self.threeMsgDisplayLabel.text == "Three Message Display"{
            return VALIDATION_ENUM.THREE_MESSEGE_DISPLAY_VALIDATION
        }else if self.yearThreeMsgDisplayBuiltLabel.text == self.yearThreeMsgDisplayDefaultText && self.threeMsgDisplayLabel.text != "0"{
            return VALIDATION_ENUM.YEAR_THREE_MSG_DISPLAY_VALIDATION
        }
        return ""
    }
    
    func setDigitalDataForDropDown(_ tag : Int,_ vc : DataPickerController){
        switch tag {
        case 0:
            vc.genderData = DROP_DOWN_ARRAYS.DIGITAL_DISPLAY_ARRAY
            vc.sourceView = BUTTON_TYPE_ENUM.DIGITAL_DISPLAY_TYPE
            break
        case 1:
            vc.genderData = DROP_DOWN_ARRAYS.YEAR_BUILT_ARRAY2
            vc.sourceView = BUTTON_TYPE_ENUM.YEAR_BUILD_DISPLAY_TYPE
            break
        case 2:
            vc.genderData = DROP_DOWN_ARRAYS.DIGITAL_DISPLAY_ARRAY
            vc.sourceView = BUTTON_TYPE_ENUM.THREE_MESSEGE_DISPLAY_TYPE
            break
        case 3:
            vc.genderData = DROP_DOWN_ARRAYS.YEAR_BUILT_ARRAY2
            vc.sourceView = BUTTON_TYPE_ENUM.YEAR_THREE_MSG_TYPE
            break
        default:
            break
        }
    }
    
    func setDropDownDataForDigital(_ selected : String , selectedIndex : Int, sourceName: String){
        if sourceName == BUTTON_TYPE_ENUM.DIGITAL_DISPLAY_TYPE{
            self.digitalDisplayLabel.text = selected
            self.digitalDisplayLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            if selected == "0"{
                self.yearDisplayBuildHeightConstraint.constant = 0
                self.yearDisplayBuiltLabel.text = self.yearDisplayDefaultText
                self.yearDisplayBuiltLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }else{
                self.yearDisplayBuildHeightConstraint.constant = 40
            }
        }else if sourceName == BUTTON_TYPE_ENUM.YEAR_BUILD_DISPLAY_TYPE{
            self.yearDisplayBuiltLabel.text = selected
            self.yearDisplayBuiltLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }else if sourceName == BUTTON_TYPE_ENUM.THREE_MESSEGE_DISPLAY_TYPE{
            self.threeMsgDisplayLabel.text = selected
            self.threeMsgDisplayLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            if selected == "0"{
                self.yearThreeMsgDisplayBuildHeightConstraint.constant = 0
                self.yearThreeMsgDisplayBuiltLabel.text = self.yearThreeMsgDisplayDefaultText
                self.yearThreeMsgDisplayBuiltLabel.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
            }else{
                self.yearThreeMsgDisplayBuildHeightConstraint.constant = 40
            }
        }else if sourceName == BUTTON_TYPE_ENUM.YEAR_THREE_MSG_TYPE{
            self.yearThreeMsgDisplayBuiltLabel.text = selected
            self.yearThreeMsgDisplayBuiltLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        }
        self.view.layoutIfNeeded()
    }
    
}
