//
//  AddSign+Identify.swift
//  SignTally
//
//  Created by Orange on 31/10/18.
//  Copyright © 2018 Orange. All rights reserved.

import Foundation
import UIKit
import CoreLocation
import GoogleMaps
import SVProgressHUD

extension AddSignController : DataPickerDelegate{
    
    func identifyViewValidations() -> String{
        if self.signOwnerLabel.text == "Sign Owner" && self.firstPhotoLabel.text == "Photo" && self.streetNumberTextFiled.text == "" && self.streetNameTextField.text == "" && self.locationNameLabel.text == "" && self.stateNameLabel.text == ""{
            return VALIDATION_ENUM.ALL_FIELD_VALIDATION_MESSEGE
        }else if self.signOwnerLabel.text == "Sign Owner"{
            return VALIDATION_ENUM.SIGN_OWNER_VALIDATION_MSG
        }else if self.signOwnerLabel.text == "Other" && self.customCompanyTextField.text == "" {
            return VALIDATION_ENUM.OTHER_SIGN_OWNER_VALIDATION_MSG
        }else if self.firstPhotoLabel.text == "Photo"{
            return VALIDATION_ENUM.SIGN_IMAGE_VALIDATION_MSG
        }else if self.streetNumberTextFiled.text == "" || self.streetNameTextField.text == "" || self.locationNameLabel.text == "" || self.stateNameLabel.text == ""{
            return VALIDATION_ENUM.LOCATION_VALIDATION_MSG
        }
        return ""
    }

    
    func pickupData(selected: String, selectedIndex: Int, sourceName: String) {
        switch self.currentIndex {
        case 0:
            self.setDropDownDataForIdentify(selected, selectedIndex: selectedIndex,sourceName: sourceName)
            break
        case 1:
            self.setDropDownDataForFeatures(selected, selectedIndex: selectedIndex, sourceName: sourceName)
            break
        case 2:
            self.setDropDownDataForDigital(selected, selectedIndex: selectedIndex, sourceName: sourceName)
            break
        default:
            break
        }
    }
    
    
    func setDropDownDataForIdentify(_ selected : String , selectedIndex : Int, sourceName: String){
        self.signOwnerLabel.text = selected
        self.signOwnerLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if selected == "Other"{
            self.customCompanyHeight.constant = 40
        }else{
            self.customCompanyHeight.constant = 0
        }
        self.view.layoutIfNeeded()
    }
}



extension AddSignController : UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate {

    func chooseSignImage() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            DispatchQueue.main.async {
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        else if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            DispatchQueue.main.async  {
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var image = info[.originalImage] as? UIImage
        image = compressImage(image: image)!
        self.selectedImage = image
        
        SVProgressHUD.show()
        AddSignWebService.uploadIamge(self.selectedImage) { (success, error, imageURL) in
            SVProgressHUD.dismiss()
            if success{
                if let imageLink = imageURL?.last{
                    if let dateTime = Date().shortHandDateTimeFormat(){
                        if self.currentImage == "Image1"{
                            self.firstPhotoLabel.text = "JPEG_\(dateTime).jpg"
                            self.firstPhotoLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                            self.firstImageLink = imageLink
                        }else{
                            self.secondPhotoLabel.text = "JPEG_\(dateTime).jpg"
                            self.secondPhotoLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                            self.secondImageLink = imageLink
                        }
                    }
                }
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "OK", vc: self)
            }
        }
        picker.dismiss(animated: true, completion: {
        })
    }
    
    
    func convertImageToBase64Format(image:UIImage?) -> String? {
        guard let _ = image else {
            return nil
        }
        let data = image!.pngData()
        return String(data: data!.base64EncodedData(options: .lineLength64Characters), encoding: String.Encoding.utf8)
    }
    
    
    // MARK: - Action Sheet
    
    
    
    func actionSheetButton(){
        //1
        let optionMenu = UIAlertController(title: nil, message: "Upload Photo", preferredStyle: .actionSheet)
        
        let takePhotoAction = UIAlertAction(title: "Take Photo", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.imagePicker.sourceType = .camera
            self.chooseSignImage()
        })
        let choosePhotoAction = UIAlertAction(title: "Choose from Gallery", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.chooseSignImage()
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        optionMenu.addAction(takePhotoAction)
        optionMenu.addAction(choosePhotoAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
}


extension AddSignController : CLLocationManagerDelegate{
    func setupLocationManager(){
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
//        self.locationManager.distanceFilter = 10
        self.locationManager.startUpdatingLocation()
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestAlwaysAuthorization()
            break
        case .authorizedAlways,.authorizedWhenInUse:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted,.denied :
            let alert = UIAlertController(title: "Location permissions are disabled for this application", message: "Please enable your location services", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Go To Settings", style: .default) { (action) in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
            }
            alert.addAction(okAction)
            present(alert, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            self.currnetLocation = location
            let locationCoordinates = location.coordinate
            if self.isFirstTime{
                self.googleReversedGeocode(locationCoordinates)
                
            }
        }
    }
    
    
    
    func googleReversedGeocode(_ location : CLLocationCoordinate2D ){
        geocode(latitude: location.latitude, longitude: location.longitude) { placemark, error in
            guard let placemark = placemark, error == nil else { return }
            // you should always update your UI in the main thread
            DispatchQueue.main.async {
                self.streetNumberTextFiled.text = placemark.subThoroughfare ?? ""
                self.streetNameTextField.text = placemark.thoroughfare ?? ""
                self.locationNameLabel.text = placemark.locality ?? ""
                print("zip code:", placemark.postalCode ?? "")
                self.zipcode = placemark.postalCode ?? ""
                self.stateNameLabel.text = placemark.administrativeArea ?? ""
                print("country:",  placemark.country ?? "",placemark.thoroughfare ?? "",placemark.subThoroughfare ?? "")
                self.isFirstTime = false
            }
        }
        
//        geocoder.reverseGeocodeCoordinate(location) { (response, error) in
//            SVProgressHUD.dismiss()
//            if let locationData = response?.results(){
//                print(locationData)
//                let address : GMSAddress = locationData[0]
//                if let locality = address.lines{
//                    let completeAddress = locality.map{String($0)}//.joined(separator: ",")
//                    print(">>>>>>\(completeAddress)")
//                    var locationArray = completeAddress[0].components(separatedBy: ",")
//                    print(locationArray)
//                    self.locationArrayGoogle = locationArray
//
//                    if locationArray[0] != ""{
//                        self.streetNumberTextFiled.text = locationArray[0]
//                    }else{
//                        self.streetNumberTextFiled.text = ""
//                    }
//                    if locationArray[1] != ""{
//                        self.streetNameTextField.text = locationArray[1]
//                    }else{
//                        self.streetNameTextField.text = ""
//                    }
//                }
//            }
//        }
    }
}
