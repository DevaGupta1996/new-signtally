//
//  ChangePasswordViewController.swift
//  SignTally
//
//  Created by orangemac05 on 29/10/18.
//  Copyright © 2018 Orange. All rights reserved.
//

import UIKit
import SVProgressHUD


class ChangePasswordViewController: UIViewController {
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var reEnterPasswordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func goBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
        let error = self.changePasswordValidation()
        if error == ""{
            self.userChangePassword()
        }else{
            showAlert(title: "SignTally", message: error, buttonTitle: "OK", vc: self)
        }
    }
    
    func changePasswordValidation() -> String{
        if self.currentPasswordTextField.text == "" && self.reEnterPasswordTextField.text == "" && self.newPasswordTextField.text == ""{
            return VALIDATION_ENUM.ALL_FIELD_VALIDATION_MESSEGE
        }
        if self.currentPasswordTextField.text == ""{
            return VALIDATION_ENUM.BLANK_CURRENT_PASSWORD_VALIDATION_MESSEGE
        }else if self.newPasswordTextField.text == ""{
            return VALIDATION_ENUM.BLANK_NEW_PASSWORD_VALIDATION_MESSEGE
        }else if self.reEnterPasswordTextField.text == ""{
            return VALIDATION_ENUM.BLANK_CONFIRM_PASSWORD_VALIDATION_MESSEGE
        }else if self.newPasswordTextField.text != self.reEnterPasswordTextField.text {
            return VALIDATION_ENUM.BLANK_CHECK_PASSWORD_VALIDATION_MESSEGE
        }
        return ""
    }
    
    
    func userChangePassword(){
        self.view.endEditing(true)
        SVProgressHUD.show()
        let params = [
            "current_password":self.currentPasswordTextField.text!,
            "user_id":USER_ID(),
            "new_password":self.newPasswordTextField.text!,
            "re_enter_password":self.reEnterPasswordTextField.text!
        ]
        ProfileServices.ChangePasswordUser(params) { (success, error, userData) in
            SVProgressHUD.dismiss()
            if success{
                if let loginData = userData?.data{
                    print(loginData)
                }
                showAlertWithCompletion(title: "SignTally", message: userData!.msg, buttonTitle: "OK", vc: self, completion: { (action) -> (Void) in
                      self.navigationController?.popViewController(animated: true)
                })
            }else{
                showAlert(title: "SignTally", message: error!, buttonTitle: "Ok", vc: self)
            }
        }
    }
}
